var express = require('express');
var app = express();

app.get('/status', function (request, response) {
    response.status(200).send();
    response.end();
});


app.get('/city', function (request, response) {
    const city = ['Paris', 'Bordeaux', 'Lyon', 'Strasbourg', 'Toulouse', 'Marseille'];
    response.json(city);
    response.end();
});

app.use(function (req, res, next) {
    res.status(404).send('Unable to find the requested resource!');
    res.end();
});

var port = process.env.PORT || 3000;
app.listen(port);